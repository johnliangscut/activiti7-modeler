package com.solace.activiti7modeler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Activiti7ModelerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Activiti7ModelerApplication.class, args);
    }

}
